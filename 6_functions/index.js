function conc (a, b) {
  return `${a}` + `${b}`;
}

let comp = function comp (a, b) {
  return (a == b) ? 1 : -1;
}

let fibonacciNumber = (num) => {
  let a = 1, b = 1;
  for (let i = 3; i <= num; i++) {
    let c = a + b;
    a = b;
    b = c;
  }
  return b;
}

let checkSpam = (request) => {
  let text = request.toLowerCase();
  return text.includes('spam') || text.includes('sex');
}

console.log(conc(1,1));
console.log(comp('abC','abc'));
console.log(fibonacciNumber(3));
console.log(fibonacciNumber(7));
console.log(fibonacciNumber(77));
console.log(checkSpam('get new Sex videos'));
console.log(checkSpam('[SPAM] How to earn fast money?'))
console.log(checkSpam('New PSD template'))
