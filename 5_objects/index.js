let ownProp = (obj, prop) => obj.hasOwnProperty(prop);

let youngPerson = () => {
  let persons = [
    { firstName : "Gosho", lastName: "Petrov", age: 32 },
    { firstName : "Bay", lastName: "Ivan", age: 81 },
    { firstname: "Kate", lastName: "Stanevko", age: 22 },
    { firstName: "Max", lastName: "Stanevko", age: 33 }
  ];

  let ag = persons.sort((a, b) => a.age - b.age)[0];

  return {
    first: ag.firstname,
    second: ag.lastName
  };
}

console.log(ownProp({'name': 'Kate', 'surname': 'Stanevko', 'age': 22}, 'age'));
console.log(ownProp({'name': 'Kate', 'surname': 'Stanevko', 'age': 22}, 'height'));
console.log(`${youngPerson().first} ${youngPerson().second}`)
