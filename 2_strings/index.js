let reverseString = (myString) => myString.split('').reverse().join('');

let bracketsValidation = (textWithBrackets) => {
  let res = 0;
  for (br of textWithBrackets) {
    (br == '(') ? res++ : (br == ')') ? res-- : res;
  }
  let answ = (res == 0) ? 'Correct' : 'Incorrect'
  return answ;
}

let wordsCount = (text, word) => (text.split(word)).length-1;

console.log(reverseString('sample'));
console.log(bracketsValidation('((a + b) / 5-d)'));
console.log(bracketsValidation(') (a + b))'));
console.log(wordsCount(`We are liv<b>in</b>g **in** an yellow submar<b>in</b>e. We don't have anyth<b>in</b>g else. **In**side the submar<b>in</b>e is very tight. So we are dr<b>in</b>k<b>in</b>g all the day. We will move out of it **in** 5 days.`,
`**in**`))
