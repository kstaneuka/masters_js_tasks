let takeLastDigit = (number) => {
  let allNumbers = {
    0: 'zero',
    1: 'one',
    2: 'two',
    3: 'three',
    4: 'four',
    5: 'five',
    6: 'six',
    7: 'seven',
    8: 'eight',
    9: 'nine'
  }

  for (num in allNumbers) {
    if ((number % 10) == num) {
      return allNumbers[num];
    }
  }
}

let reverseNumber = (number) => String(number).split('').reverse().join('');

let countWordInText = (text, word) => {
  let arr = text.split(/\W/);

  let t = arr.filter(item =>
    item == word
  );

  return t.length;
}

let countNumberInArray = (array, num) => array.filter(item => item === num).length;


console.log(takeLastDigit(512));
console.log(takeLastDigit(1024));
console.log(takeLastDigit(12309));
console.log(reverseNumber(256));
console.log(countWordInText(`Listen to your heart when he's calling for you,
Listen to your heart, there's nothing else you can do`, `heart`));
console.log(countNumberInArray([2,4,6, 8,8,8,8,8], 8))
