let arrayInit = () => {
  let array = new Array(20);
  let newArray = array.fill(0).map((item, index) => {
      return item = index * 5;
  });
  return newArray;
}

let maxSequence = (array) => {
  let seq = [array[0]];
  let tempSeq = [array[0]];

  for (let i = 1; i < array.length; i++) {
    if (array[i] == array[i - 1]) {
      tempSeq.push(array[i]);
    } else if (tempSeq.length > seq.length) {
      seq = tempSeq;
    } else {
      tempSeq = [array[i]];
    }
  }
  if (tempSeq.length > seq.length) {
    seq = tempSeq;
  }
  return seq;
}

let maxIncSequence = (array)  => {
  let seq = [array[0]];
  let tempSeq = [array[0]];

  for (let i = 1; i < array.length; i++) {
    if (array[i] > array[i - 1]) {
      tempSeq.push(array[i]);
    } else if (tempSeq.length > seq.length) {
      seq = tempSeq;
    } else {
      tempSeq = [array[i]];
    }
  }
  if (tempSeq.length > seq.length) {
    seq = tempSeq;
  }
  return seq;
}

let sort = (array) => array.sort((a, b) => (a > b) ? 1 : -1);

let maxNumCount = (array) => {
  let count = 1, maxCount, num = array[0];
  for (let i = 0; i < array.length - 1; i++) {
    maxCount = 1
    for (let j = i + 1; j < array.length; j++) {
      if (array[i] == array[j]) {
        maxCount += 1;
      }
    }
    if (maxCount > count) {
      count = maxCount;
      num = array[i];
    }
  }
  return {
    max: count,
    elem: num
  };
}

let arrayComp = (firstArray, secondArray) => {
let result = 0, letter;
for (let i = 0; i < firstArray.length; ++i) {
  letter = firstArray[i];
  for (let j = i; j < secondArray.length; ++j) {
    if (letter == secondArray[j]) {
      result ++;
      console.log(`Coinsidence in : ${letter}`);
      break;
    }
  }
}
if (result == firstArray.length || result == firstArray.length) {
  console.log(`firstArray == secondArray`);
} else {
  console.log(`firstArray != secondArray`);
}
return result;
}

console.log(`Arrays initialization = ${arrayInit()}`);
console.log(`Max sequence = ${maxSequence([2, 1, 1, 2, 3, 3, 2, 2, 2, 1])}`);
console.log(`Max increasing sequence = ${maxIncSequence([3, 2, 3, 4, 2, 2, 4])}`);
console.log(`Sorted array = ${sort([10,9,8,7,6,5,4,3,2,1])}`);
console.log(`Number = ${maxNumCount([4, 1, 1, 4, 2, 3, 4, 4, 1, 2, 4, 9, 3]).elem}, Max number count = ${maxNumCount([4, 1, 1, 4, 2, 3, 4, 4, 1, 2, 4, 9, 3]).max}`);
console.log(`Number of coinciding letters = ${arrayComp(['a', 'b','m','y'],['a','b','m','y'])}`);
console.log(`Number of coinciding letters = ${arrayComp(['a', 's','t','y'],['a','b','t','y'])}`)
